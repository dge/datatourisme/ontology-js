/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

var Ontology = require('../src/ontology.js');
var obj = require('./fixtures/ontology.json');


describe("ontology suite", function() {
    var ontology = Ontology.fromJson(obj);

    it("load json obj", function() {
        /*ontology.getClass(":PlaceOfInterest").getSuperClasses().forEach(function(c) {
            console.log(c.getUri());
        });

        console.log(ontology.getClass(":City").isCandidateClassOf(":PointOfInterest"));*/

        //var ind = ontology.getIndividual("http://www.datatourisme.fr/resource/core/1.0#66039");
        //console.log(ind.getLabel());
        //ind.getTypes().forEach(function(c) {
        //    console.log(c.getUri());
        //});


        ontology.getClass(":Audience").getIndividuals().forEach(function(c) {
           console.log(c.getLabel());
        });

        //expect(ontology).toBe(true);
    });
});
    